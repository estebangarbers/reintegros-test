<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('crons/respuesta','CronController@respuesta')->name('crons.respuesta');

Route::any('crons/cronCrearProyectos', 'CronController@cronCrearProyectos')
->name('crons.cronCrearProyectos');

Route::any('crons/cronConsultarExpediente', 'CronController@cronConsultarExpediente')
->name('crons.cronConsultarExpediente');

Route::any('crons/cronCrearGedos', 'CronController@cronCrearGedos')
->name('crons.cronCrearGedos');

Route::any('crons/cronVincularGedosCertiAExpediente', 'CronController@cronVincularGedosCertiAExpediente')
->name('crons.cronVincularGedosCertiAExpediente');

Route::any('crons/cronNotificarGEDOaTAD', 'CronController@cronNotificarGEDOaTAD')
->name('crons.cronNotificarGEDOaTAD');

Route::any('crons/cronCleanSesiones', 'CronController@cronCleanSesiones')
->name('crons.cronCleanSesiones');

Route::any('crons/cronGenerarPase', 'CronController@cronGenerarPase')
->name('crons.cronGenerarPase');

Route::any('crons/cronGuardaTemporal', 'CronController@cronGuardaTemporal')
->name('crons.cronGuardaTemporal');

Route::any('crons/cronVincularRespaldoYGenerarPase', 'CronController@cronVincularRespaldoYGenerarPase')
->name('crons.cronVincularRespaldoYGenerarPase');

Route::middleware(['CheckCuit'])->group(
	function ()
	{
		Route::get('solicitud/sLocalidad','SolicitudController@searchLocalidad');
		Route::any('/', 'SolicitudController@inicio')->name('solicitud.inicio');
		Route::get('/solicitudes', 'SolicitudController@solicitudes')->name('solicitud.listado');
		Route::get('solicitud/envio', 'SolicitudController@envio')->name('solicitud.envio');
		Route::post('solicitud/view', 'SolicitudController@view')->name('solicitud.view')->middleware('CheckAuthSolicitud');
		Route::post('solicitud/copy', 'SolicitudController@copy')->name('solicitud.copy')->middleware('CheckAuthSolicitud');
		Route::any('solicitud/test', 'SolicitudController@test')->name('solicitud.test');
		Route::resource('solicitud', 'SolicitudController');
	}
);
