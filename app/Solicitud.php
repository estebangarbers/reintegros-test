<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
	protected $table = 'solicitudes';

    protected $fillable = ['id', 'domicilio', 'localidad_id', 'provincia_id', 'producto_id', 'nrestablecimiento', 'cdeclarada', 'paisdestino_id', 'parancelaria', 'descripcion', 'mtransporte', 'ntransporte', 'vfob', 'otros_registros', 'destinacion', 'email', 'telefono', 'tercero_id', 'sesion_id', 'created_at', 'updated_at'];

    protected $hidden = [
        'empresa_id',
    ];

    public function empresa()
    {
        return $this->belongsTo('Reintegros\Empresa');
    }

    public function establecimiento()
    {
        return $this->hasOne('Reintegros\Establecimiento', 'id', 'nrestablecimiento');
    }

    public function expediente()
    {
        return $this->hasOne('Reintegros\Expediente', 'solicitud_id');
    }

    public function gedo()
    {
        return $this->hasOne('Reintegros\Gedo', 'solicitud_id');
    }

    public function gedoRespaldo()
    {
        return $this->hasOne('Reintegros\GedoRespaldo', 'solicitud_id');
    }

    public function gedoFFCC()
    {
        return $this->hasOne('Reintegros\GedoFFCC', 'solicitud_id');
    }

    public function sesion()
    {
        return $this->belongsTo('Reintegros\Sesion');
    }

    public function producto()
    {
        return $this->hasOne('Reintegros\Producto', 'id', 'producto_id');
    }

    public function pais_destino()
    {
        return $this->hasOne('Reintegros\Pais', 'id', 'paisdestino_id', 'nombre');
    }

    public function localidad()
    {
        return $this->hasOne('Reintegros\Localidad', 'id', 'localidad_id', 'nombre');
    }

    public function provincia()
    {
        return $this->hasOne('Reintegros\Provincia', 'id', 'provincia_id', 'nombre');
    }

    public function tercero()
    {
        return $this->hasOne('Reintegros\Tercero', 'id', 'tercero_id');
    }
}
