<?php

namespace Reintegros\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    						'/', 
    						'solicitud', 
    						'solicitud/view',
    						'crons/respuesta', 
    						'crons/cronCrearProyectos', 
    						'crons/cronConsultarExpediente', 
    						'crons/cronCrearGedos', 
    						'crons/cronVincularGedosCertiAExpediente', 
    						'crons/cronCleanSesiones', 
    						'crons/cronNotificarGEDOaTAD', 
    						'crons/cronGenerarPase',
                            'crons/cronGuardaTemporal',
                            'crons/cronVincularRespaldoYGenerarPase' 						
    					];
}
