<?php

namespace Reintegros\Http\Middleware;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Carbon\Carbon;
use Reintegros\Empresa;
use Reintegros\Solicitud;
use Reintegros\Sesion;

use Closure;
use Flash;
use Session;

class CheckAuthSolicitud
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    //protected $except = ['solicitud/respuesta', 'solicitud/test', 'solicitud/cron', 'solicitud/cronRespuesta'];
    public function handle($request, Closure $next)
    {
        // cierra sesion
        if ($request->solicitud_id && $request->session()->get('sesion_id')) 
        {
            try 
            {
                $solicitud = Solicitud::where('id', '=', $request->solicitud_id)->firstOrFail();
                $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->first();
            }
            catch (ModelNotFoundException $e)
            {
                $request->session()->flush();
                return abort(404, 'ERROR – Ocurrió un error de autenticación, por favor ingrese al sistema nuevamente a través de TAD.');
            }

            if ($solicitud->empresa_id != $sesion->empresa->id) 
            {
                $request->session()->flush();
                return abort(404, 'ERROR – Ocurrió un error de autenticación, por favor ingrese al sistema nuevamente a través de TAD.');
            }

            return $next($request);
        }
    }
}
