<?php

namespace Reintegros\Http\Middleware;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Carbon\Carbon;
use Reintegros\Empresa;
use Reintegros\Sesion;

use Closure;
use Flash;
use Session;

class CheckCuit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $except = ['solicitud/respuesta', 'solicitud/test', 'solicitud/cron', 'solicitud/cronRespuesta'];
    public function handle($request, Closure $next)
    {
        //dd($request->server);
        //usar $_SERVER["REMOTE_USER"]=> string(11) "27254361122"
        if ($request->logout) 
        {
            $request->session()->flush();
            return redirect('http://www.alimentosargentinos.gob.ar/');
        }

        if ($request->cuit || $request->query('cuit'))
        {
            try 
            {
                $empresa = Empresa::where('cuit', '=', $request->cuit)->where('activo', 1)->firstOrFail();
            }
            catch (ModelNotFoundException $e)
            {
                $request->session()->flush();
                return abort(404, 'ERROR – El CUIT recibido no se encuentra registrado en nuestro sistema o no se encuentra autorizado a ingresar.');
            }

            // chequeo que tenga productos vigentes
            if ($empresa->productos()->where('vigente', '>=', Carbon::today()->toDateString())->get()->isEmpty())
            {
                $request->session()->flush();
                return abort(404, 'ERROR - El CUIT recibido no posee productos vigentes registrados en nuestro sistema.');
            }

            // chequeo que tenga registros de establecimiento
            if ($empresa->establecimientos()->get()->isEmpty()) 
            {
                $request->session()->flush();
                return abort(404, 'ERROR - El CUIT recibido no posee registros de establecimiento cargados.');
            }

            // todo OK, almaceno en sesion
            $sesion = new Sesion();
            $sesion->empresa_id = $empresa->id;
            $sesion->last_activity = Carbon::now()->timestamp;
            $sesion->save();
            session(['sesion_id' => $sesion->id]);
            return $next($request);            
        }

        // si tiene sesion iniciada, renueva ultima actividad
        if ($request->session()->get('sesion_id'))
        {
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->first();
            $sesion->last_activity = Carbon::now()->timestamp;
            $sesion->save();
            return $next($request);
        }



        $request->session()->flush();
        return abort(404, 'Inicie sesión a través de TAD para continuar.');   
    }
}
