<?php

namespace Reintegros\Http\Controllers;

use Reintegros\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public static function get($empresa_id){
    	$provincias = Provincia::all();
        $select_provincia = [];
        $select_provincia[''] = "Seleccione una provincia";
        foreach($provincias as $provincia){
            $select_provincia[$provincia->id] = $provincia->nombre;
        }
        return $select_provincia;
    }
}
