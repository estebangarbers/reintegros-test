<?php

namespace Reintegros\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Reintegros\Http\Controllers\LinkGDE;
use Reintegros\Http\Controllers\CronController;

use Reintegros\Empresa;
use Reintegros\Producto;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;
use Reintegros\Expediente;
use Reintegros\Sesion;
use Reintegros\Gedo;
use Reintegros\GedoFFCC;
use Reintegros\GedoRespaldo;

use PDF;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $linkgde;
    protected $usuarioGDE;
    protected $usuarioGDESecundario;

    protected $codigoProyecto;
    protected $urlProyecto;
    protected $urlResponse;
    protected $urlCrearDocumento;
    protected $urlCrearDocumentoFC;
    protected $vincularDocumento;
    protected $consultarExpediente;
    protected $areaPase;

    public function __construct() {
        $this->linkgde = new LinkGDE();
        $this->usuarioGDE = '';
        $this->usuarioGDESecundario = '';
        $this->codigoProyecto = '';
        $this->urlProyecto = '';
        $this->urlResponse = '';
        $this->urlCrearDocumento = '';
        $this->urlCrearDocumentoFC = '';
        $this->vincularDocumento = '';
        $this->consultarExpediente = '';

        $this->areaPase = 'DI#MA';
    }

    protected function getCodigoProyecto() {
        return $this->codigoProyecto;
    }

    protected function getUrlProyecto() {
        return $this->urlProyecto;
    }

    protected function getUrlResponse() {
        return $this->urlResponse;
    }

    protected function getUrlCrearDocumento() {
        return $this->urlCrearDocumento;
    }

    protected function getAreaPase() {
        return $this->areaPase;
    }

    /*
    |--------------------------------------------------------------------------
    | Crear Proyecto (Expediente)
    |--------------------------------------------------------------------------
    |
    | Crea el proyecto en la BD, guarda el numero "idProyecto" que servirá de referencia cuando el 
    | sistema reciba la respuesta.
    |
    */
    protected function generarProyecto($solicitud)
    {
        $idProyecto = $this->linkgde->crearProyecto($solicitud, $this->usuarioGDE);
        if ($idProyecto != 0)
        {
            $expediente = new Expediente();
            $expediente->idProyecto = $idProyecto;
            $expediente->solicitud_id = $solicitud->id;
            $save = $expediente->save();
            if($save) return true;
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | Crear GEDO Certificación
    |--------------------------------------------------------------------------
    |
    | Recibe el objeto solicitud, crea el certificado en formato PDF y envia la información a 
    | linkGDE para crear el FC.
    | Retorna json ($data) con el numero de FC creado, el mismo se inserta en base de datos
    | con vinculación a la solicitud en cuestión. 
    |
    */
    protected function crearGedoCerti($solicitud)
    {
        $nombre = "Certificado de Reintegro Nro ".str_pad($solicitud->id, 8, "0", STR_PAD_LEFT);

        if (!$solicitud->gedo)
        {
            // GEDO CON CERTIFICADO ADJUNTO PARA NOTIFICAR
            $pdf = PDF::loadView('pdf.certificados', compact('solicitud'));
            $archivo = $pdf->output();
            $acronimo = "CERTI";
            $data = $this->linkgde->crearDocumentoGedo($archivo, 
                                                       $nombre, 
                                                       $acronimo,
                                                       $this->usuarioGDE);
            if($data)
            {
                $gedo = new Gedo();
                $gedo->licencia = $data['licencia'];
                $gedo->numero = $data['numero'];
                $gedo->numeroEspecial = $data['numeroEspecial'];
                $gedo->urlArchivoGenerado = $data['urlArchivoGenerado'];
                $gedo->solicitud_id = $solicitud->id;
                $save = $gedo->save();
                if(!$save) return false;
            }
        }

        return true;      
    }

    /*
    |--------------------------------------------------------------------------
    | Crear Formularios Controlados
    |--------------------------------------------------------------------------
    |
    | Recibe el objeto solicitud, crea PDF y envia la información a linkGDE para crear el FC.
    | Retorna json con el numero de FC creado, el mismo se inserta en base de datos con vinculacion 
    | a la solicitud en cuestion. 
    |
    */
    protected function crearGedoFFCC($solicitud)
    {
        if (!$solicitud->gedoFFCC)
        {
            $nombre = "Certificado de Reintegro Nro ".str_pad($solicitud->id, 8, "0", STR_PAD_LEFT);
            $archivo = null;

            $data = $this->linkgde->crearDocumentoFC($solicitud, 
                                                     $archivo, 
                                                     $nombre, 
                                                     $this->usuarioGDE);

            if($data)
            {
                $gedo = new GedoFFCC();
                $gedo->licencia = $data['licencia'];
                $gedo->numero = $data['numero'];
                $gedo->numeroEspecial = $data['numeroEspecial'];
                $gedo->urlArchivoGenerado = $data['urlArchivoGenerado'];
                $gedo->solicitud_id = $solicitud->id;
                $save = $gedo->save();
                if(!$save) return false;
            }
        }

        return true;      
    }

    protected function crearGedoRespaldo($solicitud)
    {
        $nombre = "Certificado de Reintegro Nro ".str_pad($solicitud->id, 8, "0", STR_PAD_LEFT);

        if (!$solicitud->gedoRespaldo)
        {
            // GEDO CON CERTIFICADO ADJUNTO PARA RESPALDO
            // DEBE FIRMARLO OTRO AGENTE DIFERENTE AL QUE FIRMA EL FC
            $pdf = PDF::loadView('pdf.solicitudes', compact('solicitud'));
            $archivo = $pdf->output();

            $acronimo = "PCIUD";
            $data = $this->linkgde->crearDocumentoGedo($archivo, 
                                                        $nombre, 
                                                        $acronimo,
                                                        $this->usuarioGDESecundario);

            if($data)
            {
                $gedo = new GedoRespaldo();
                $gedo->licencia = $data['licencia'];
                $gedo->numero = $data['numero'];
                $gedo->numeroEspecial = $data['numeroEspecial'];
                $gedo->urlArchivoGenerado = $data['urlArchivoGenerado'];
                $gedo->solicitud_id = $solicitud->id;
                $save = $gedo->save();
                if (!$save) return false;
            }
        }

        return true;      
    }

    /*
    |--------------------------------------------------------------------------
    | Vincular FC a Expediente
    |--------------------------------------------------------------------------
    | Recibe el objeto solicitud y el numero de expediente (String)
    | Si es linkGDE retorna true, modifica el estado del GEDO por 1 (Vinculado) en base de datos
    |
    */
    protected function vincularGedoExpediente($documento, $expediente)
    {
        if ($documento && $expediente) 
        {
            $vinculacion = $this->linkgde->vincularGedoAExpediente(array($documento), 
                                                                    $expediente, 
                                                                    $this->usuarioGDE);
            
            if ($vinculacion) return true;
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | notificarGEDOaTAD (Expediente)
    |--------------------------------------------------------------------------
    |
    | Crea el proyecto en la BD, guarda el numero "idProyecto" que servirá de referencia cuando el 
    | sistema reciba la respuesta.
    |
    */
    protected function notificarGEDOaTAD($gedo)
    {
        # e.g.: IF-2018-49229403-APN-SECMA#MPY
        preg_match('/(.*)-(.*)-(.*)-(.*)-(.*)/', $gedo->numero, $matches);
        $actuacion = $matches[1];
        $anio = $matches[2];
        $numero_gedo = $matches[3];
        $reparticion = $matches[5];
        $motivo = 'Para su trámite';
        $cuit = $gedo->solicitud->empresa->cuit;
        $expediente = $gedo->solicitud->expediente->nroCompleto;

        return $this->linkgde->NotificarTAD(
            $expediente,
            $actuacion,
            $anio,
            $numero_gedo,
            $reparticion,
            $motivo,
            $cuit,
            $this->usuarioGDE
        );
    }
}
