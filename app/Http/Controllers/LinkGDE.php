<?php
namespace Reintegros\Http\Controllers;

use Illuminate\Http\Request;
use Reintegros\Http\Controllers\RestRequest;

class LinkGDE extends Controller
{
    protected $codigoProyecto;
    protected $urlProyecto;
    protected $urlResponse;
    protected $urlCrearDocumento;
    protected $urlBloquear;
    protected $urlDesbloquear;
    protected $urlEstadoBloqueo;
    protected $urlGenerarPase;
    protected $urlGenerarPaseBloqueo;
    protected $urlGenerarPaseDesbloqueo;
    protected $vincularDocumento;
    protected $consultarExpediente;

    /*
    ws_gde_usuario_existe: 'http://linkgde.magyp.gob.ar/usuarios/getusuario' // pa que lo tengas
    */

    public function __construct() {
        // $request->server->get('REMOTE_USER');
        $this->codigoProyecto = 'MAGR00069';  //PROD: MAGR00093
        $this->urlResponse = '';
        $this->urlProyecto = '';      
        $this->urlCrearDocumento = '';
        $this->urlCrearDocumentoFC = '';
        $this->urlVincularDocumentoYConfirmar = '';
        $this->urlBloquear = '';
        $this->urlDesbloquear = '';
        $this->urlEstadoBloqueo = '';
        $this->urlGenerarPase = '';
        $this->urlGenerarPaseBloqueo = '';
        $this->urlGenerarPaseDesbloqueo = '';
        $this->urlNotificarTAD = '';
        $this->consultarExpediente = '';
    }

    // CREA CARATULA - DEVUELVE IDPROYECTO
    public function crearProyecto($solicitud, $usuario) {
        $datosGDE = [
            'titulo' => 'Certificado de reintegro a las exportaciones de '.$solicitud->empresa->razon_social,
            'texto' => 'Certificado de Reintegro Nro '. $solicitud->id,
            'areaDestinataria' => 'DI#MA', // DNAYB#MPTY (Hay que cambiar buzon nuevo)
            'codigoTramite' => $this->codigoProyecto,
            'externo' => 'true',
            'cuit' => $solicitud->empresa->cuit,
            'usuario' => strtoupper($usuario),
            'sistema' => 'SELLOS',
            'token' => '',
            'urlResponse' => $this->urlResponse,
        ];

        $rest = new RestRequest($this->urlProyecto, 'POST', $datosGDE);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            $data = $respuesta['Data'];
            if ($data['idProyecto'] == -1) {
                return 0;
            }

            return $data['idProyecto'];
        }

        return 0;
    }

    // CREA GEDO
    public function crearDocumentoGedo($archivo, $nombre, $acronimo, $usuarioGDE) {
        if (is_null($usuarioGDE)) {
            return 0;
        }
        $archivoEnBase64 = base64_encode($archivo);
        $datosDocumento = [
            'Reparticion' => 'DI#MA',//'DNAYB#MPYT',
            'Nombre' => $nombre,
            'Acronimo' => $acronimo,
            'Data' => $archivoEnBase64, //Archivo en base64
            'SistemaOrigen' => 'MA_PROYECTOS',
            'Referencia' => $nombre,
            'TipoArchivo' => 'pdf',
            'Usuario' => strtoupper($usuarioGDE),
            'Cargo' => 'TECNICO', //cargo del usuario
        ];

        $rest = new RestRequest($this->urlCrearDocumento, 'POST', $datosDocumento);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            $data = $respuesta['Data'];
            $datos = [
                'licencia' => $data['licenciaField'], // esto nos vino siempre null
                'numero' => $data['numeroField'], // esto es lo importantisimo
                'numeroEspecial' => $data['numeroEspecialField'], // esto nos vino siempre null 
                'urlArchivoGenerado' => $data['urlArchivoGeneradoField'], // esto creo que es info dentro de GDE
            ];

            return $datos;
        }

        return 0;
    }

    // CREA DOCUMENTO FC
    public function crearDocumentoFC($solicitud, $archivo, $nombre, $usuarioGDE) {
        if (is_null($usuarioGDE)) 
        {
            return 0;
        }

        $certificacion = ($solicitud->producto->certificacion == 1 ? "Sello Alimentos Argentinos" : "Denominación de Origen e Indicación Geográfica");
        
        $archivoEnBase64 = base64_encode($archivo);
        $datosComponente = [
			'txt_razon_social' => $solicitud->empresa->razon_social,
            'txt_domicilio' => $solicitud->domicilio,
            'txt_localidad' => ($solicitud->localidad ? $solicitud->localidad->nombre : ''),
            'txt_provincia' => $solicitud->provincia->nombre,
            'txt_producto' => $solicitud->producto->nombre,
            'txt_marca' => $solicitud->producto->marca,
            'txt_tipo_n_registro' => ($solicitud->producto->tipo_registro && $solicitud->producto->nrproducto ? $solicitud->producto->tipo_registro.' '.$solicitud->producto->nrproducto : ''),
            'txt_n_resolucion' => $solicitud->producto->nresolucion,
            'txt_certificacion' => $certificacion,
            'txt_n_registro' => ($solicitud->nrestablecimiento ? $solicitud->nrestablecimiento : ''),
            'txt_otros_registros' => ($solicitud->otros_registros ? $solicitud->otros_registros : ''),
            'txt_destinacion' => $solicitud->destinacion,
            'txt_repita_destinacion' => $solicitud->destinacion,
            'txt_cantidad_declarada' => $solicitud->cdeclarada,
            'txt_pais_destino' => $solicitud->pais_destino->nombre,
            'txt_posicion_arancelaria' => $solicitud->parancelaria,
            'txt_valor_fob' => $solicitud->vfob,
            'txt_medio_transporte' => $solicitud->mtransporte,
            'txt_nombre_transporte' => $solicitud->ntransporte,
            'txt2500_descripcion' => $solicitud->descripcion,
            'txt_correo_electronico' => $solicitud->email,
            'txt_telefono' => $solicitud->telefono
		];

		$datosDocumento = [
            'Acronimo' => "FOSLL", // CRSOC?
            'Referencia' => $nombre,
            'SistemaOrigen' => 'MA_PROYECTOS',
            'Usuario' => '',
            'Data' => $archivoEnBase64,
            'Componentes' => $datosComponente,
        ];

        $respuesta = json_decode($this->curlSpecial($datosDocumento, $this->urlCrearDocumentoFC), true);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) 
        {
            $data = $respuesta['Data']['return'];
            $datos = [
                'licencia' => $data['licenciaField'], // esto nos vino siempre null
                'numero' => $data['numeroField'], // esto es lo importantisimo
                'numeroEspecial' => $data['numeroEspecialField'], // esto nos vino siempre null 
                'urlArchivoGenerado' => $data['urlArchivoGeneradoField'], // esto creo que es info dentro de GDE
            ];

            return $datos;
        }

        return 0;
    }

    public function vincularGedoAExpediente($documentos, $expediente, $usuarioGDE) {

        $datosDocumento = [
            'Usuario' => strtoupper($usuarioGDE),
            'SistemaOrigen' => 'MA_PROYECTOS',
            'Expediente' => $expediente,
            'Documentos' => $documentos,
        ];

        $respuesta = json_decode($this->curlSpecial($datosDocumento, $this->urlVincularDocumentoYConfirmar), true);

        if ((JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) ||
            ('El documento ya se encuentra vinculado al expediente.' == $respuesta['Mensaje']))
        {
            return true;
        }

        return false;
    }

    public function consultarExpediente($idProyecto) {
        $ws = $this->consultarExpediente . "?idProyecto=$idProyecto";
        $rest = (new RestRequest($ws, 'GET', null))->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);
        if (array_key_exists('nroCompleto', $respuesta) && null != $respuesta['nroCompleto']) {
            $expediente = [
                'nroCompleto' => $respuesta['nroCompleto'],
                'numero'      => $respuesta['numero'],
                'ano'         => $respuesta['ano'],
            ];
            return $expediente;
        }
        return null;
    }

    public function bloquearExpediente($expediente) 
    {
        $body = [
            "sistema" => "MA_PROYECTOS",
            "numeroExpediente" => $expediente,
        ];

        $rest = new RestRequest($this->urlBloquear, 'POST', $body);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);

        if ($respuesta['Status'] == 'success') {
            return true;
        }

        return 0;
    }

    public function desbloquearExpediente($expediente) 
    {
        $body = [
            "sistema" => "MA_PROYECTOS",
            "numeroExpediente" => $expediente,
        ];

        $rest = new RestRequest($this->urlDesbloquear, 'POST', $body);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);

        if ($respuesta['Status'] == 'success') {
            return true;
        }

        return 0;
    }

    public function generarPaseConBloqueo($expediente, $estado, $areaDestino, $usuarioGDE) {
        $body = [
            "codigoEE"             => $expediente,
            "estadoSeleccionado"   => $estado,
            "motivoPase"           => $estado == 'Guarda Temporal' ? 'Guarda temporal' : 'Pase para su tramitación',
            "reparticionDestino"   => $areaDestino,
            "sectorDestino"        => "PVD",
            "sistemaOrigen"        => "MA_PROYECTOS",
            "usuarioDestino"       => "",
            "usuarioOrigen"        => $usuarioGDE,
            "esReparticionDestino" => false,
            "esSectorDestino"      => $estado == 'Guarda Temporal' ? false : true,
            "esMesaDestino"        => false,
            "esUsuarioDestino"     => false
        ];

        $respuesta = json_decode($this->curlSpecial($body, $this->urlGenerarPaseBloqueo), true);

        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            return true;
        }

        return false;
    }

    public function estadoBloqueo($expediente_id) {
        $expediente_id = urlencode($expediente_id);
        $ws = $this->urlEstadoBloqueo . "?numeroExpediente=$expediente_id";
        $rest = (new RestRequest($ws, 'GET', null))->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);

        return $respuesta['Data'];
    }

    public function notificarTAD($expediente_id, $actuacion, $anio, $numero_gedo, $reparticion, $motivo, $cuit, $usuario) {
        $body = [
            "actuacion" => $actuacion,
            "anio" => $anio,
            "numero" => $numero_gedo,
            "reparticion" => $reparticion,
            "motivo" => $motivo,
            "codigoExpediente" => $expediente_id,
            "usuarioCreacion" => $usuario,
            "cuit" => $cuit,
        ];
        //dd($body);
        $rest = new RestRequest($this->urlNotificarTAD, 'POST', $body);
        $rest->execute();
        $respuesta = json_decode($rest->getResponseBody(), true);

//        dd($respuesta);
        if (JSON_ERROR_NONE == json_last_error() && is_array($respuesta) &&
                array_key_exists('Status', $respuesta) && 'success' == $respuesta['Status']) {
            return true;
        }
        
        return false;
    }

    public function prueba(){
        return "asd";
    }

    private function curlSpecial($datos, $url)
    {
        $json = json_encode($datos);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS,$json); 
        $data = curl_exec($ch);  
        curl_close($ch); 
        return $data;
    }

}
