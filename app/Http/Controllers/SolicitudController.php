<?php
namespace Reintegros\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Reintegros\Http\Controllers\RestRequest;
use Reintegros\Http\Controllers\LinkGDE;

use Reintegros\Pais;
use Reintegros\Provincia;
use Reintegros\Localidad;

use Reintegros\Empresa;
use Reintegros\Producto;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;
use Reintegros\Expediente;
use Reintegros\Sesion;
use Reintegros\Gedo;
use Reintegros\GedoFFCC;
use Reintegros\GedoRespaldo;

use Carbon\Carbon;

use Flash;
use Session;
use PDF;

class SolicitudController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Pagina inicial
    |--------------------------------------------------------------------------
    |
    | A esta pagina debe estar dirigido PAEC, o autogestion segun corresponda.
    |
    */    
    public function inicio(Request $request)
    {
        try 
        {
            // TRAE SESION CREADA EN MIDDLEWARE
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – El CUIT recibido no se encuentra registrado en nuestro sistema');
        }

        // seteo empresa
        $empresa = $sesion->empresa;

        return view('inicio', compact('empresa'));
    }

    /*
    |--------------------------------------------------------------------------
    | Pagina inicial
    |--------------------------------------------------------------------------
    |
    | Esta página muestra la solicitudes registradas por el CUIT con sesión activa.
    |
    */ 
    public function solicitudes(Request $request)
    {
        try
        {
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();

        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'Inicie sesión a través de TAD para continuar.');
        }

        // seteo empresa
        $empresa = $sesion->empresa;
        $solicitudes = Solicitud::where('empresa_id','=',$sesion->empresa->id)->orderBy('id', 'DESC')->paginate(10);
        return view('solicitud.listado', compact('empresa', 'solicitudes'));
    }
     
    
    public function index(Request $request)
    {
        try 
        {
            // TRAE SESION CREADA EN MIDDLEWARE
            $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'Inicie sesión a través de TAD para continuar.');
        }

        // seteo empresa
        $empresa = $sesion->empresa;

        // traigo los establecimientos relacionados de *rexp_establecimientos*
        $establecimientos = $sesion->empresa->establecimientos()->pluck('registro','registro');

        // traigo terceros, si posee
        if (!$empresa->terceros->where('estado', 1)->isEmpty()) {
            $terceros = $empresa->terceros->where('estado', 1)->mapWithKeys(function ($item) {
                return [$item['id'] => $item['cuit'].' - '.$item['razon_social']];
            });
        }else{
            $terceros = null;
        }
        
        // traigo los productos relacionados de *rexp_productos*
        try 
        {
            // TRAE SESION CREADA EN MIDDLEWARE
            $productos_vigentes = Producto::where('empresa_id', '=', $empresa->id)->where('vigente', '>=', Carbon::today()->toDateString())->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'Inicie sesión a través de TAD para continuar.');
        }

        $productos = collect($productos_vigentes)->mapWithKeys(function ($item) {
            return [$item['id'] => $item['nombre'].' 
            | '.($item['tipo_registro'] && $item['nrproducto'] ? $item['tipo_registro'].''.$item['nrproducto'].' 
            | ' : '').$item['nresolucion'].' 
            | '.$item['marca']];
        });
        

        // traigo las posiciones arancelarias de *rexp_parancelarias*
        $parancelarias = collect(Parancelaria::all())->mapWithKeys(function ($item) {
            return [$item['numero'] => $item['numero'].' 
            | '.$item['producto']];
        });

        // traigo las posiciones arancelarias de *rexp_paises*
        $paises = Pais::where('alpha_3','!=','arg')->pluck('nombre','id');
        // traigo las posiciones arancelarias de *rexp_provincias*
        $provincias = Provincia::all()->pluck('nombre','id');

        // COPIAR FORMULARIO
        if ($request->copy=='true' && $request->session()->get('post')) {
            $post = collect($request->session()->get('post'));
        }else{
            $post = null;
        }

        return view('solicitud.index', compact('empresa','establecimientos','productos','parancelarias','paises', 'provincias','post', 'terceros'));
    }

    /*
    |--------------------------------------------------------------------------
    | Carga de solicitud
    |--------------------------------------------------------------------------
    |
    | Se carga la solicitud enviada por el usuario.
    | Se carga el GEDO una vez la solicitud esté en BD mediante Formulario Controlado (FC)
    |
    */
    public function store(Request $request)
    {
        if ($request->input() && $request->session()->get('sesion_id')) 
        {
            try 
            {
                $sesion = Sesion::where('id', '=', $request->session()->get('sesion_id'))->firstOrFail();
            }
            catch (ModelNotFoundException $e)
            {
                return abort(404, 'ERROR 404 – Ocurrió un error con la sesión.');
            }

            $solicitud = new Solicitud();
            $solicitud->empresa_id = $sesion->empresa_id;
            $solicitud->domicilio = $request->input('domicilio');
            $solicitud->localidad_id = $request->input('localidad_id');
            $solicitud->provincia_id = $request->input('provincia_id');
            $solicitud->producto_id = $request->input('producto_id');
            $solicitud->nrestablecimiento = $request->input('nrestablecimiento');
            $solicitud->cdeclarada = str_replace(",", ".", 
                                     str_replace(".", "", $request->input('cdeclarada')));
            $solicitud->paisdestino_id = $request->input('paisdestino_id');
            $solicitud->parancelaria = $request->input('parancelaria');
            $solicitud->descripcion = $request->input('descripcion');
            $solicitud->mtransporte = $request->input('mtransporte');
            $solicitud->ntransporte = $request->input('ntransporte');
            $solicitud->vfob = str_replace(",", ".", 
                               str_replace(".", "", $request->input('vfob')));
            $solicitud->otros_registros = $request->input('otros_registros');
            $solicitud->destinacion = $request->input('destinacion');
            $solicitud->email = $request->input('email');
            $solicitud->telefono = str_replace("-", "", $request->input('ctelefono'))."-".str_replace("-", "", $request->input('ntelefono'));
            if (is_numeric($request->input('tercero_id'))) {
            $solicitud->tercero_id = $request->input('tercero_id');
            }
            $solicitud->sesion_id = $request->session()->get('sesion_id');
            $result = $solicitud->save();

            if(!$result)
            {
                return abort(500, 'ERROR 500 – Ocurrió un error en el servidor al intentar cargar la solicitud.');
            }

            $this->generarProyecto($solicitud);
            $this->crearGedoRespaldo($solicitud);
            $this->crearGedoFFCC($solicitud);
            $this->crearGedoCerti($solicitud);
            // GUARDO POST ENVIADO EN SESION
            session(['post' => $request->input()]);

            return redirect('solicitud/envio');
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Mensaje satisfactorio de registro de la solicitud
    |--------------------------------------------------------------------------
    |
    | Lo único que hace es devolver una vista con mensaje satisfactorio.
    | Para evitar mantener el POST mediante la redireccion.
    |
    */
    public function envio(Request $request)
    {
        try
        {
            $sesion = Sesion::find($request->session()->get('sesion_id'))->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404, 'ERROR 404 – Ocurrió un error con la sesión.');
        }

        Flash::success('La solicitud ha sido registrada correctamente.');
        $empresa = $sesion->empresa;
        // ENVIAR MAIL?
        
        return view('solicitud.envio', compact('empresa'));
    }

    /*
    |--------------------------------------------------------------------------
    | Buscar localidad
    |--------------------------------------------------------------------------
    |
    | Busca localidades con los parametros recibidos p y q. p => Provincia, q => Query
    | Función que se utiliza para seleccionar localidad en el formulario de la solicitud.
    |
    */
    public function searchLocalidad(Request $request)
    {
        if ($request->p && $request->q) 
        {
            $localidades = \DB::table('localidades')
                ->join('partidos', 'partidos.id', '=', 'localidades.partido_id')
                ->select('localidades.id as localidad_id','localidades.nombre as localidad_nombre','partidos.id as partido_id','partidos.nombre as partido_nombre')
                ->where('partidos.provincia_id','=',$request->p)
                ->where('localidades.nombre','LIKE','%'.$request->q.'%')
                ->get();

            return json_encode($localidades);
        }
        
        return null;
    }

    public function view(Request $request){
        try
        {
            $solicitud = Solicitud::where('id','=',$request->solicitud_id)->firstOrFail();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        $pdf = PDF::loadView('pdf.solicitudes', compact('solicitud'));
        return $pdf->download($solicitud->empresa->cuit."-".$solicitud->id.".pdf");
    }

    public function copy(Request $request)
    {
        try
        {
            $solicitud = Solicitud::where('id','=',$request->solicitud_id)->firstOrFail(); 
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if ($solicitud->localidad) 
        {
           $solicitud->q = $solicitud->localidad->nombre.", ".$solicitud->localidad->partido->nombre;
        }
        else
        {
            $solicitud->q = null;
        }
        
        $solicitud->rdestinacion = $solicitud->destinacion;
        $telefono = explode('-', $solicitud->telefono, 2);
        $solicitud->ctelefono = $telefono[0];
        $solicitud->ntelefono = $telefono[1];
        session(['post' => $solicitud]);
        return \Redirect::route('solicitud.index', ['copy' => 'true']);
    }

    /*
    |--------------------------------------------------------------------------
    | Ruta para testear
    |--------------------------------------------------------------------------
    */
    public function test(Request $request)
    {
        $solicitud = Solicitud::where('id','=',79)->firstOrFail(); 
        $pdf = PDF::loadView('pdf.certificados', compact('solicitud'));
        return $pdf->stream();
    }
}
