<?php
namespace Reintegros\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Reintegros\Http\Controllers\RestRequest;
use Reintegros\Http\Controllers\LinkGDE;
use Reintegros\Http\Controllers\SolicitudController;

use Reintegros\Pais;
use Reintegros\Provincia;
use Reintegros\Localidad;

use Reintegros\Empresa;
use Reintegros\Producto;
use Reintegros\Parancelaria;
use Reintegros\Solicitud;
use Reintegros\Expediente;
use Reintegros\Sesion;
use Reintegros\Gedo;
use Reintegros\GedoFFCC;
use Reintegros\GedoRespaldo;

use Carbon\Carbon;

use Flash;
use Session;
use PDF;

class CronController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Respuesta GDE
    |--------------------------------------------------------------------------
    |
    | Se recibe la respuesta del sistema. La funcion busca el idProyecto y le carga los datos 
    | recibidos [nroCompleto, numero, ano].
    |
    */
    public function respuesta(Request $request) 
    {
        $params = [];
        $content = $request->getContent();

        if (!empty($content)) 
        {
            $params = json_decode($content, true);
            
            if(JSON_ERROR_NONE == json_last_error() && is_array($params))
            {
                $idProyecto = $params['idProyecto'];
                try 
                {
                    $expediente = Expediente::where('idProyecto', '=', $idProyecto)->firstOrFail();
                }
                catch (ModelNotFoundException $e)
                {
                    return null;
                }
                                
                // MODIFICO CON LA INFORMACION RECIBIDA DEL SISTEMA
                $expediente->nroCompleto = $params['nroCompleto'];
                $expediente->numero = $params['numero'];
                $expediente->ano = $params['ano'];
                $save = $expediente->save();
            }
        }

        return null;
    }
    

    /*
    |------------------------------------------------------------------------------------------
    | CRONJOB para crear Proyecto en caso que haya fallado al crear la solicitud
    |------------------------------------------------------------------------------------------
    |
    | Selecciona las solicitudes que NO tengan expediente en BD.
    | Crea los proyectos que falten y los guarda en la base de datos con su idProyecto.
    |
    */
    public function cronCrearProyectos(Request $request)
    {
        try
        {
            $solicitudes = Solicitud::whereNotIn('id', function($q)
            {
                $q->select('solicitud_id')->from('expedientes');
            })
            ->whereHas('sesion', function ($q){
                $q->where(function($q){
                    $q->where('sesions.last_activity','<',Carbon::now()->subMinutes(31)->timestamp);
                });
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$solicitudes->isEmpty())
        {
            foreach ($solicitudes as $solicitud) 
            {
                // CREO PROYECTO
                $this->generarProyecto($solicitud);
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para crear los Gedos
    |--------------------------------------------------------------------------
    |
    | Se consulta a la base de datos y se traen las solicitudes a las que le falten algún Gedo (Respaldo, | Certi y/o FFCC).
    | En caso que existan solicitudes, se crea el Gedo que faltase.
    */
    public function cronCrearGedos(Request $request){
        try
        {
            $solicitudes = Solicitud::whereNotIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_respaldo');
            })
            ->OrWhereNotIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_ffcc');
            })
            ->OrWhereNotIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos');
            })
            ->get(); 
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$solicitudes->isEmpty()) 
        {
            foreach ($solicitudes as $solicitud) 
            {
                if (!$solicitud->gedoRespaldo) {
                    $this->crearGedoRespaldo($solicitud);
                }

                if (!$solicitud->gedo) {
                    $this->crearGedoCerti($solicitud);
                }

                if (!$solicitud->gedoFFCC) {
                    $this->crearGedoFFCC($solicitud);
                }
            }
        }

        return ;
    }

    /*
    |-------------------------------------------------------------------------------
    | CRONJOB para vincular el Gedo Respaldo al Expediente y Generar pase del mismo
    |-------------------------------------------------------------------------------
    |
    | Se consulta la base de datos y se traen las solicitudes con Expediente con nroCompleto asignado y 
    | sin pase realizado. Además, solicitudes con Gedo Respaldo creado y que no haya sido vinculado.
    | A las solicitudes traídas, se vincula el Gedo Respaldo al Expediente y se genera el pase.
    */
    public function cronVincularRespaldoYGenerarPase(Request $request){
        try
        {
            $solicitudes = Solicitud::whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('expedientes')
                ->where('nroCompleto','!=',"")
                ->orWhereNotNull('nroCompleto')
                ->where('pase','=',"0");
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_respaldo')
                ->whereNotNull('numero')
                ->where('vinculado','=',"0");
            })->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$solicitudes->isEmpty()) 
        {
            foreach ($solicitudes as $solicitud) 
            {
                $result = $this->vincularGedoExpediente($solicitud->gedoRespaldo->numero,
                                                        $solicitud->expediente->nroCompleto);

                if ($result) {
                    $solicitud->gedoRespaldo->vinculado = 1;
                    $solicitud->gedoRespaldo->save();
                }

                if ($solicitud->gedoRespaldo->vinculado == 1) {
                    $this->linkgde->bloquearExpediente($solicitud->expediente->nroCompleto);
                    $pase = $this->linkgde->generarPaseConBloqueo($solicitud->expediente->nroCompleto, 
                                                                  "Tramitación", 
                                                                  $this->areaPase, 
                                                                  $this->usuarioGDE);
                    if ($pase) {
                        $solicitud->expediente->pase = 1;
                        $solicitud->expediente->save();
                    }
                }
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para vincular Gedos FFCC y CERTI a Expediente
    |--------------------------------------------------------------------------
    |
    | Selecciona las solicitudes que tengan Expediente completo, con pase realizado y con gedos CERTI y/o | FFCC sin vinculación realizada.
    | Con las solicitudes obtenidas, se vincula lo que falte vincular.
    |
    */
    public function cronVincularGedosCertiAExpediente()
    {
        try
        {
            $solicitudes = Solicitud::whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('expedientes')
                ->where('pase','=',1)
                ->where('nroCompleto','!=',"")
                ->orWhereNotNull('nroCompleto');
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_respaldo')
                ->where('vinculado','=',1);
            })
            ->whereRaw("(`id` in (select `solicitud_id` from `rexp_gedos` where `vinculado` = 0) OR `id` in (select `solicitud_id` from `rexp_gedos_ffcc` where `vinculado` = 0))")
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$solicitudes->isEmpty()) 
        {
            foreach ($solicitudes as $solicitud) 
            {

                if ($solicitud->gedoFFCC->vinculado == 0) {
                    $result = $this->vincularGedoExpediente($solicitud->gedoFFCC->numero,
                                                            $solicitud->expediente->nroCompleto);

                    if ($result) {
                        $solicitud->gedoFFCC->vinculado = 1;
                        $solicitud->gedoFFCC->save();
                    }
                }

                if ($solicitud->gedo->vinculado == 0) {

                    $result = $this->vincularGedoExpediente($solicitud->gedo->numero,
                                                            $solicitud->expediente->nroCompleto);

                    if ($result) {
                        $solicitud->gedo->vinculado = 1;
                        $solicitud->gedo->save();
                    }
                }
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para notificar GEDOS CERTI a TAD
    |--------------------------------------------------------------------------
    |
    | Selecciona las solicitudes con todos los GEDOS creados y vinculados y no han sido notificados a TAD
    |
    */
    public function cronNotificarGEDOaTAD(Request $request)
    {
        try
        {
            $solicitudes = Solicitud::whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('expedientes')
                ->where('pase','=',1)
                ->where('nroCompleto','!=',"")
                ->orWhereNotNull('nroCompleto');
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos')
                ->where('vinculado','=',1)
                ->where('notificado','=',0);
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_ffcc')
                ->where('vinculado','=',1);
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_respaldo')
                ->where('vinculado','=',1);
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$solicitudes->isEmpty()) 
        {
            foreach ($solicitudes as $solicitud) 
            {
                $notificar = $this->notificarGEDOaTAD($solicitud->gedo);
                if($notificar)
                {
                    $solicitud->gedo->notificado = 1;
                    $solicitud->gedo->save();
                }
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB Guarda Temporal
    |--------------------------------------------------------------------------
    |
    | Se genera un pase a Guarda Temporal de las solicitudes que ya hayan terminado su proceso.
    | Se obtienen las solicitudes con todos los GEDOS generados, vinculados y GEDO CERTI notificado.
    | Este método, en true, actualizá el estado "PASE" al valor "2" a los Expedientes.
    |
    */
    public function cronGuardaTemporal(Request $request)
    {
        try
        {
            $solicitudes = Solicitud::whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('expedientes')
                ->where('pase','=',1);
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos')
                ->where('vinculado','=',1)
                ->where('notificado','=',1);
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_ffcc')
                ->where('vinculado','=',1);
            })
            ->whereIn('id', function($q)
            {
                $q->select('solicitud_id')->from('gedos_respaldo')
                ->where('vinculado','=',1);
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        /*
        Reparticiones:
        SAYBI#MA-PVD SECRETARIA
        DNAYB#MA-PVD DIRECCION NACIONAL
        DPAYB#MA-PVD DIRECCION
        */
        if (!$solicitudes->isEmpty()) {
            foreach ($solicitudes as $solicitud) {
                $this->linkgde->bloquearExpediente($solicitud->expediente->nroCompleto);
                $result = $this->linkgde->generarPaseConBloqueo($solicitud->expediente->nroCompleto, 
                                                        "Guarda Temporal",
                                                        $this->areaPase, 
                                                        $this->usuarioGDE);
                if($result){
                    $solicitud->expediente->pase = 2;
                    $solicitud->expediente->save();
                }                
            }
        }

        return null;
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para consultar estado del Expediente
    |--------------------------------------------------------------------------
    |
    | Se consulta manualmente si ya se creo el expediente y nuestro sistema no fue notificado.
    | En caso afirmativo, modifica la fila de nuestra BD con la informacion, chequea que esten todos los 
    | gedos y vincula los gedos al expediente
    |
    */
    public function cronConsultarExpediente()
    {
        // Selecciona expedientes que no esten completos, es decir, que no hayan recibido respuesta
        try 
        {
            $expedientes = Expediente::where('nroCompleto', '=', '')
                                ->orWhereNull('nroCompleto')
                                ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if ($expedientes) 
        {
            foreach ($expedientes as $expediente)
            {
                $params = $this->linkgde->consultarExpediente($expediente->idProyecto);
                if (is_array($params))
                {
                    $expediente->nroCompleto = $params['nroCompleto'];
                    $expediente->numero = $params['numero'];
                    $expediente->ano = $params['ano'];
                    $expediente->save();
                }
            }   
        }

        return null;  
    }

    /*
    |--------------------------------------------------------------------------
    | CRONJOB para limpiar sesiones almacenadas en base de datos
    |--------------------------------------------------------------------------
    |
    | Selecciona las sesiones que caducaron sin solicitudes vinculadas y las borra.
    |
    */
    public function cronCleanSesiones()
    {
        try
        {
            /* SELECT SESIONES FINALIZADAS SIN SOLICITUDES VINCULADAS */
            $sesiones = Sesion::where('last_activity','<',Carbon::now()->subMinutes(31)->timestamp)
            ->whereNotIn('id', function($q)
            {
                $q->select('sesion_id')->from('expedientes');
            })
            ->whereNotIn('id', function($q)
            {
                $q->select('sesion_id')->from('solicitudes');
            })
            ->get();
        }
        catch (ModelNotFoundException $e)
        {
            return abort(404);
        }

        if (!$sesiones->isEmpty())
        {
            foreach ($sesiones as $sesion) 
            {
                $sesion->delete();
            }
        }

        return null;
    }

}
