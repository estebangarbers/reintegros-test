<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Tercero extends Model
{
    protected $table = 'terceros';

	protected $fillable = ['id','empresa_id','razon_social','cuit','created_at','updated_at'];
}
