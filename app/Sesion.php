<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Sesion extends Model
{
    protected $table = 'sesions';

	protected $fillable = ['id','empresa_id','created_at','updated_at'];

    protected $primaryKey = 'id';

	public function empresa()
    {
        return $this->belongsTo('Reintegros\Empresa');
    }

    public function solicitudes()
    {
        return $this->hasMany('Reintegros\Solicitud', 'sesion_id');
    }
}
