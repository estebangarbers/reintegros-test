<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    protected $table = 'establecimientos';

	protected $fillable = ['id','empresa_id','registro','created_at','updated_at'];
}
