<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
	protected $table = 'localidades';

	protected $fillable = ['id','nombre','provincia_id','partido_id'];

	public function partido()
    {
        return $this->hasOne('Reintegros\Partido', 'id', 'partido_id');
    }
}
