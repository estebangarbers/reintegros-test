<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
    protected $table = 'expedientes';

	protected $fillable = ['idProyecto','nroCompleto','numero','ano','solicitud_id','created_at','updated_at'];

	protected $primaryKey = 'idProyecto';

	public function solicitud()
    {
        return $this->belongsTo('Reintegros\Solicitud');
    }

}
