<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';

	protected $fillable = ['id','razon_social','cuit', 'vigente', 'created_at','updated_at'];

    public function establecimientos()
    {
    	return $this->hasMany('Reintegros\Establecimiento', 'empresa_id');
    }

    public function terceros()
    {
        return $this->hasMany('Reintegros\Tercero', 'empresa_id');
    }

    public function productos()
    {
    	return $this->hasMany('Reintegros\Producto', 'empresa_id');
    }

    public function solicitudes()
    {
        return $this->hasMany('Reintegros\Solicitud', 'empresa_id')->orderBy('id', 'desc');
    }

    public function sesiones()
    {
        return $this->hasMany('Reintegros\Sesion', 'empresa_id');
    }

    /* public function destinos(){
        return $this->belongsToMany('Integral\Pais', 'pymes_destinos')->withTimestamps();
    }

    public function provincia(){
        return $this->hasOne('Integral\Provincia');
    } */
}
