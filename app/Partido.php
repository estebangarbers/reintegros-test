<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
	protected $table = 'partidos';

	protected $fillable = ['id','nombre','id_provincia','orden'];

	public function localidades()
    {
        return $this->hasMany('Reintegros\Localidad', 'partido_id');
    }
}
