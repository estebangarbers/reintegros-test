<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Parancelaria extends Model
{
    protected $table = 'parancelarias';

	protected $fillable = ['numero','producto'];
}
