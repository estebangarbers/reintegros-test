<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Gedo extends Model
{
    protected $table = 'gedos';

	protected $fillable = ['licencia','numero','numeroEspecial','urlArchivoGenerado','vinculado', 'notificado','estado','solicitud_id','created_at','updated_at'];

	//protected $primaryKey = 'numero';

	public function solicitud()
    {
        return $this->belongsTo('Reintegros\Solicitud');
    }
}
