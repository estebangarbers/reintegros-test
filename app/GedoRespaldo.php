<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class GedoRespaldo extends Model
{
    protected $table = 'gedos_respaldo';

	protected $fillable = ['licencia','numero','numeroEspecial','urlArchivoGenerado','vinculado', 'notificado','estado','ffcc','solicitud_id','created_at','updated_at'];

	//protected $primaryKey = 'numero';

	public function solicitud()
    {
        return $this->belongsTo('Reintegros\Solicitud');
    }
}
