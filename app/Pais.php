<?php

namespace Reintegros;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
	protected $table = 'paises';

	protected $fillable = ['id','nombre','alpha_2','alpha_3'];

	public function solicitudes()
    {
        return $this->hasMany('Reintegros\Solicitud');
    }
}