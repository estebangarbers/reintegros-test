@extends('layouts.app')
@section('content')
<main class="container">
<section class="row">
  <div class="col-md-10 col-md-offset-1">
    <h1>Solicitud de reintegros a las exportaciones</h1>
    <hr> 
    <h2 class="titulo">{{ $empresa->razon_social }}</h2>
    <p class="text-muted subtitulo">CUIT: {{ $empresa->cuit }}</p>
    <p><b>Solicitudes registradas:</b></p>
    @if(!$solicitudes->isEmpty())
    <table class="table">
      <tbody>
        @foreach($solicitudes as $solicitud)
        <tr>
          <td>Solicitud N°{{str_pad($solicitud->id, 8, "0", STR_PAD_LEFT)}}</td>
          <td class="text-right"><u>Registrada el día:</u> {{ $solicitud->created_at->format('d/m/Y H:i') }}hs</td>
          <td width="1%">
            {!! Form::open(['method' => 'POST', 'route' => 'solicitud.copy', 'style' => 'margin:0']) !!}
            <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
            <button type="submit" class="btn btn-warning btn-sm" style="margin:0;"><i class="fa fa-copy"></i> Copiar</button>
            {!! Form::close() !!}
          </td>
          <td width="1%">
            {!! Form::open(['method' => 'POST', 'route' => 'solicitud.view', 'style' => 'margin:0']) !!}
            <input type="hidden" name="solicitud_id" value="{{$solicitud->id}}">
            <button type="submit" class="btn btn-primary btn-sm" style="margin:0;"><i class="fa fa-download"></i> Descargar</button>
            {!! Form::close() !!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$solicitudes->render()}}
    @else
    <div class="alert alert-warning">No posee solicitudes registradas en el sistema.</div>
    @endif
    
    <div class="row">
      <div class="col-md-12">
        <a href="{{route('solicitud.inicio')}}" class="btn btn-link pull-left">Volver al inicio</a>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <hr>
        <p> 
          <img src="{{url('img/logo_ministerio2.png')}}" alt="Logo organismo" width="400">
        </p>
      </div>
    </div>

  </div>
</section>
</main>
@endsection
