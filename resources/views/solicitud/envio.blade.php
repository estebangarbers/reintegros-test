@extends('layouts.app')

@section('content')

<main class="container">
<section class="row">

  <div class="col-md-10 col-md-offset-1">
    <h1>Solicitud de reintegros a las exportaciones</h1>
    <hr> 
    <h2 class="titulo">{{ $empresa->razon_social }}</h2>
    <p class="text-muted subtitulo">CUIT: {{ $empresa->cuit }}</p>

    @include('flash::message')
    
    <div class="row">
      <div class="col-md-9">
        <a href="{{route('solicitud.index')}}" class="btn btn-success m-1-1">Registrar otra solicitud</a>
        <a href="{{route('solicitud.index',['copy' => 'true'])}}" class="btn btn-warning m-1-1"><i class="fa fa-copy"></i> Copiar</a>
      </div>
      <div class="col-md-3">
        <a href="{{route('solicitud.inicio')}}" class="btn btn-link pull-right">Volver al inicio</a>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <hr>
        <p> 
          <img src="{{url('img/logo_ministerio2.png')}}" alt="Logo organismo" width="400">
        </p>
      </div>
    </div>

  </div>
</section>
</main>

@endsection
