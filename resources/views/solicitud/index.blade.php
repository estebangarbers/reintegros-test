@extends('layouts.app')

@section('content')
<div id="pageloader">
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
</div>
<main class="container">
<section class="row">
  
  <div class="col-md-10 col-md-offset-1">
    <h1>Solicitud de reintegros a las exportaciones</h1>    
    <hr> 
    <h2 class="titulo">{{ $empresa->razon_social }}</h2>
    <p class="text-muted subtitulo">CUIT: {{ $empresa->cuit }}</p>
    {!! Form::open(['route' => 'solicitud.store', 'method' => 'POST', 'id' => 'form_solicitud']) !!}
      <!-- UBICACIÓN -->
    @if($terceros)
      <div class="row">
        <div class="col-md-12 form-group item-form">
          {{ Form::label('tercero_id', '¿Exporta por si?. En caso contraro seleccione una razón social') }}
          {{ Form::select('tercero_id', $terceros, $post['tercero_id'], ['class' => 'form-control','id' => 'tercero_id','placeholder' => 'Si']) }}
        </div>
      </div>
      <hr style="margin-top:0;">
      @endif
      <fieldset>        
        <legend><h3>Ubicación</h3></legend>
        <div class="row">
          <div class="col-md-5 form-group item-form">
            {{ Form::label('provincia', 'Provincia') }}
            {{ Form::select('provincia_id', $provincias, $post['provincia_id'], ['class' => 'form-control','id' => 'provincia_id','placeholder' => 'Seleccione una provincia']) }}
          </div>
          <div class="col-md-7 form-group item-form" id="cont-localidad">
            <input type="hidden" name="q">
            {{ Form::label('localidad', 'Localidad') }}
            <div class="typeahead__container">
              <div class="typeahead__field">
                <div class="typeahead__query">
                  {{ Form::search('q', $post['q'], ['class' => 'js-typeahead form-control', 'id' => 'localidad', 'autocomplete' => 'off', 'placeholder' => 'Escriba la localidad y seleccione de la lista desplegable']) }}
                  <span id="localidad-error-dos" class="error" for="localidad" style="display:none;">Por favor ingrese una localidad y seleccione del desplegable</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 form-group item-form">
            {{ Form::label('domicilio', 'Domicilio') }}
            {{ Form::text('domicilio', $post['domicilio'], ['class' => 'form-control']) }}
          </div>
        </div>
        {{ Form::hidden('localidad_id', $post['localidad_id'], ['class' => 'form-control', 'id' => 'localidad_id']) }}
      </fieldset>

      <!-- DATOS DEL PRODUCTO -->
      <fieldset>
        <legend><h3>Información del producto</h3></legend>
        <div class="row">
          <div class="col-md-12 form-group item-form">
            {{ Form::label('producto_id', 'Producto') }}
            {{ Form::select('producto_id', $productos, $post['producto_id'], ['class' => 'form-control','id' => 'producto_id','placeholder' => 'Seleccione un producto']) }}
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group item-form">
              {{ Form::label('nrestablecimiento', 'N° registro establecimiento') }}
              {{ Form::select('nrestablecimiento', $establecimientos, $post['nrestablecimiento'], ['class' => 'form-control','placeholder' => 'Seleccione un registro']) }}
            </div>
          </div>
          <div class="col-md-6">
            {{ Form::label('otros_registros', 'Otros registros según el producto (OPCIONAL)') }}
            {{ Form::text('otros_registros', $post['otros_registros'], ['class' => 'form-control', 'id' => 'otros_registros']) }}
          </div>
        </div>    


        <div class="row">
          <div class="col-md-6">
            <div class="form-group item-form">
              {{ Form::label('destinacion', 'Destinación de exportación') }}
              {{ Form::text('destinacion', $post['destinacion'], ['class' => 'form-control', 'id' => 'destinacion']) }}
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group item-form">
              {{ Form::label('destinacion', 'Repita la destinación de exportación') }}
              {{ Form::text('rdestinacion', $post['rdestinacion'], ['class' => 'form-control', 'id' => 'rdestinacion']) }}
              <span id="destinacion-error-dos" class="error" for="rdestinacion" style="display:none;">Las destinaciones deben coincidir</span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <legend style="margin-bottom:5px;">Cantidad declarada <small class="text-muted">(neto en toneladas)</small></legend>
            <div class="form-group item-form">
              <div>
                {{ Form::text('cdeclarada', $post['cdeclarada'], ['class' => 'form-control', 'id' => 'cdeclarada', 'style' => 'padding-right:95px', 'placeholder' => '0,000']) }}
                <span style="position:absolute;right:30px;top:43px;">Toneladas</span>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group item-form">
              {{ Form::label('paisdestino_id', 'País de destino') }}
              {{ Form::select('paisdestino_id', $paises, null, ['class' => 'form-control','placeholder' => 'Seleccione un país']) }}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 form-group item-form">
            {{ Form::label('parancelaria', 'Posición arancelaria (8 dígitos) NCM') }}
            {{ Form::select('parancelaria', $parancelarias, $post['parancelaria'], ['class' => 'form-control','placeholder' => 'Seleccione una posición']) }}
          </div>
          <div class="col-md-6">
            <legend style="margin-bottom:5px;">Valor FOB <small class="text-muted">(en Dólares Estadounidenses)</small></legend>
            <div class="form-group item-form">
              <div>
                {{ Form::text('vfob', $post['vfob'], ['class' => 'form-control', 'id' => 'vfob', 'style' => 'padding-right:55px', 'placeholder' => '0,00']) }}
                <span style="position:absolute;right:30px;top:43px;">U$S</span>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group item-form">
              {{ Form::label('mtransporte', 'Medio de transporte') }}
              {{ Form::select('mtransporte', ['Marítimo' => 'Marítimo','Fluvial' => 'Fluvial','Aéreo' => 'Aéreo','Terrestre' => 'Terrestre'], $post['mtransporte'], ['class' => 'form-control','placeholder' => 'Seleccione un medio de transporte']) }}
            </div>
          </div>
          <div class="col-md-6">
            {{ Form::label('ntransporte', 'Nombre del transporte') }}
            {{ Form::text('ntransporte', $post['ntransporte'], ['class' => 'form-control', 'id' => 'ntransporte']) }}
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 form-group item-form">
            {{ Form::label('descripcion', 'Descripción de la mercadería') }}
            {{ Form::textarea('descripcion', $post['descripcion'], ['class' => 'form-control', 'id' => 'descripcion', 'rows' => 5]) }}
          </div>
        </div>
      </fieldset>

      <!-- DATOS DE CONTACTO -->
      <fieldset>
        <legend><h3>Datos de contacto</h3></legend>        
        <div class="row"> 
          <div class="col-md-6 form-group item-form">
            {{ Form::label('email', 'Dirección de correo electrónico') }}
            {{ Form::email('email', $post['email'], ['class' => 'form-control', 'id' => 'email']) }}
          </div>
          <div class="col-sm-6">
            <legend style="margin-bottom:5px;">Teléfono <small class="text-muted">(Incluí el <a href="https://www.argentina.gob.ar/codigos-de-area-telefonicos-de-argentina" aria-label="Ingresá código de área. Si no lo sabés consultá en este enlace" target="_blank" tabindex="-1">código de área</a> de tu localidad)</small></legend>
            <div class="row">
              <div class="form-group col-xs-12 col-sm-3 item-form">
                <div>
                  {{ Form::text('ctelefono', $post['ctelefono'], ['class' => 'form-control', 'id' => 'ctelefono', 'style' => 'padding-left:20px;padding-right:17px', 'maxlength' => 5]) }}
                  <span style="position:absolute;left:25px;top:16px;">(</span>
                  <span style="position:absolute;right:25px;top:16px;">)</span>
                </div>
              </div>
              <div class="form-group col-xs-12 col-sm-9 item-form">
                {{ Form::text('ntelefono', $post['ntelefono'], ['class' => 'form-control', 'id' => 'ntelefono']) }}
              </div>
            </div>
          </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <input type="hidden" name="empresa_id" value="{{$empresa->id}}">
          <a href="{{route('solicitud.inicio')}}" class="btn btn-link">Volver al inicio</a> 
          {{ Form::submit('Registrar solicitud', ['class' => 'btn btn-success m-l-1 pull-right']) }}
        </div>
      </div>
    </fieldset>
{{Form::close()}}

  <div class="row">
    <div class="col-md-12">
      <hr>
      <p> 
        <img src="{{url('img/logo_ministerio2.png')}}" alt="Logo organismo" width="400">
      </p>
    </div>
  </div>

  </div>
</section>
</main>

@endsection
