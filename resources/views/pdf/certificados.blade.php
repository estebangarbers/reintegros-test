@extends('layouts.pdf')

@section('content')
    <div class="cuerpo">
        <p>CERTIFICACIÓN:</p>
        <p>La DIRECCIÓN NACIONAL DE ALIMENTOS Y BEBIDAS de la SECRETARÍA de ALIMENTOS Y BIOECONOMÍA de la SECRETARÍA DE GOBIERNO DE AGROINDUSTRIA certifica que {{mb_strtoupper($solicitud->empresa->razon_social, 'UTF-8')}}, con CUIT N° {{$solicitud->empresa->cuit}} resulta beneficiario del derecho de uso del Sello "ALIMENTOS ARGENTINOS UNA ELECCIÓN NATURAL", y su versión en idioma inglés "ARGENTINE FOOD, A NATURAL CHOICE".</p>
        Para el producto: {{$solicitud->producto->nombre}}.<br/>
        Registro de Producto: {{($solicitud->producto->nrproducto ? $solicitud->producto->tipo_registro.' '.$solicitud->producto->nrproducto : $solicitud->producto->marca)}}.<br/>
        Registro de Establecimiento N°: {{$solicitud->nrestablecimiento}}.<br/>
        Comercializado bajo la/s marca/s: {{$solicitud->producto->marca}}.<br/>
        <p>Reconocido mediante Resolución {{$solicitud->producto->nresolucion}} y se encuentra vigente.</p>
        <p><b>Producto, características y marcas de la producción</b></p>
        Destinación de exportación por la que solicita el beneficio: {{$solicitud->destinacion}}<br/>
        Peso neto en toneladas: {{str_replace('.', ',', $solicitud->cdeclarada)}}<br/>
        Valor FOB expresado en Dólares Estadounidenses: U$S {{str_replace('.', ',', $solicitud->vfob)}}<br/>
        País de destino: {{$solicitud->pais_destino->nombre}}<br/>
        Posición arancelaria a 8 dígitos con las siglas NCM: {{$solicitud->parancelaria}}<br/>
        Descripción de la mercadería: {{$solicitud->descripcion}}<br/>
        <p>El presente certificado deberá presentarse dentro de los 45 días corridos contados a partir del libramiento de la mercadería, siendo condición ineludible su presentación en dicho plazo para el cobro del reintegro adicional y es emitido para ser presentado ante DIRECCIÓN GENERAL DE ADUANAS (DGA) a los fines de lo establecido en el Artículo 4° o 5° del Decreto N° 1341 de fecha 30 de diciembre de 2016.</p>
        Fecha: {{$solicitud->created_at->format('d/m/Y H:i') }}hs<br/>
        N° solicitud: {{str_pad($solicitud->id, 8, "0", STR_PAD_LEFT)}}<br/>
        </div>
    </div>
@endsection