@extends('layouts.app')

@section('content')

<main class="container">
<section class="row">
  
  <div class="col-md-10 col-md-offset-1">
    <h1>Solicitud de reintegros a las exportaciones</h1>
    <p>Solicitud de Certificado de Reintegro en función del Decreto N° 1341/16 para Productos Orgánicos, Sello Alimentos Argentinos y Sistema de Denominaciones de Origen e Indicación Geográfica.</p>
    <hr> 
    <h2 class="titulo">{{ $empresa->razon_social }}</h2>
    <p class="text-muted subtitulo">CUIT: {{ $empresa->cuit }}</p>    
    <div class="row">
        <div class="col-md-6">
            <a class="panel panel-default" href="{{ route('solicitud.listado') }}">
              <div class="panel-body">
                <h3><i class="fa fa-eye"></i> Ver solicitudes generadas</h3>
              </div>
            </a>
        </div>

        <div class="col-md-6">
            <a class="panel panel-default" href="{{ route('solicitud.index') }}">
              <div class="panel-body">
                <h3><i class="fa fa-plus"></i> Registrar nueva solicitud</h3>
              </div>
            </a>
        </div>

        <div class="col-md-12">
            <div class="alert alert-info">Por problemas y/o sugerencias puede enviar un mail a <a href="mailto:reintegros@magyp.gob.ar">reintegros@magyp.gob.ar</a> o llamar al +54 (011) 4349-2041.</div>
            <a href="{{url('/?logout=true')}}" class="btn btn-danger pull-right">Cerrar sesión</a>
        </div>



    </div>

    
    

    <div class="row">
    <div class="col-md-12">
      <hr>
      <p> 
        <img src="{{url('img/logo_ministerio2.png')}}" alt="Logo organismo" width="400">
      </p>
    </div>
    </div>

  </div>
</section>
</main>

@endsection