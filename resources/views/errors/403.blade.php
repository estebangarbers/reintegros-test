@extends('layouts.app')

@section('content')
<main class="container">
<section class="row">
	<div class="col-md-12 col-md-offset-0">
		<div class="alert alert-danger">{{ $exception->getMessage() }}</div>
		<p>Por problemas y/o sugerencias puede enviar un mail a <a href="mailto:reintegros@magyp.gob.ar">reintegros@magyp.gob.ar</a> o llamar al +54 (011) 4349-2041</p>
	</div>
</section>
@endsection