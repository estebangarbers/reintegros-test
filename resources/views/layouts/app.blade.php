<html class="no-js">
        <head>
            <meta charset="UTF-8">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <title>Solicitud de reintegro a las exportaciones</title>
            <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/ico" />
            <link href="{{ asset('css/roboto-fontface.css') }}" rel="stylesheet">
            <link href="{{ asset('css/droid-serif.css') }}" rel="stylesheet">
            <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
            <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
            <link rel="stylesheet" href="{{ asset('css/poncho.css') }}">
            <link rel="stylesheet" href="{{ asset('css/jquery.typeahead.css') }}">

            <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
            <script src="{{ asset('js/jquery.validate.js') }}"></script>
            <script type="text/javascript">
                $().ready(function() {
                    $("#form_solicitud").validate({
                        rules: {
                            provincia_id: "required",
                            q: "required",
                            localidad_id: "required",
                            domicilio: "required",
                            producto_id: "required",
                            nrestablecimiento: "required",
                            destinacion: "required",
                            rdestinacion: {
                              equalTo: "#destinacion",
                              required: true
                            },
                            cdeclarada: "required",
                            paisdestino_id: "required",
                            parancelaria: "required",
                            vfob: "required",
                            mtransporte: "required",
                            ntransporte: "required",
                            descripcion: {
                                required: true,
                                maxlength: 2500
                            },
                            email: {
                                required: true,
                                email: true
                            },
                            ctelefono: "required",
                            ntelefono: "required",
                        },
                        messages: {
                            provincia_id: "Por favor seleccione una provincia",
                            q: {
                                required: "Por favor ingrese una localidad y seleccione del desplegable"
                            },
                            domicilio: {
                                required: "Por favor ingrese un domicilio"
                            },
                            producto_id: {
                                required: "Por favor seleccione un producto"
                            },
                            nrestablecimiento: {
                                required: "Por favor seleccione un establecimiento"
                            },
                            destinacion: {
                                required: "Por favor ingrese la destinación"
                            },
                            rdestinacion: {
                                required: "Por favor repita la destinación",
                                equalTo: "Por favor introduzca la misma destinación"
                            },
                            cdeclarada: {
                                required: "Por favor ingrese la cantidad declarada"
                            },
                            paisdestino_id: {
                                required: "Por favor seleccione el país de destino"
                            },
                            parancelaria: {
                                required: "Por favor seleccione la posición arancelaria"
                            },
                            vfob: {
                                required: "Por favor ingrese el valor FOB"
                            },
                            mtransporte: {
                                required: "Por favor seleccione el medio de transporte"
                            },
                            ntransporte: {
                                required: "Por favor ingrese el nombre del transporte"
                            },
                            descripcion: {
                                required: "Por favor ingrese una descripción de la mercadería"
                            },
                            email: {
                                required: "Por favor ingrese un correo electrónico de contacto",
                                email: "Por favor ingrese un correo electrónico válido"
                            },
                            ctelefono: {
                                required: "Requerido"
                            },
                            ntelefono: {
                                required: "Por favor ingrese un número de teléfono"
                            },
                        }
                    });
                });
            </script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#form_solicitud').submit(function (event) {
                        if (!$("#localidad_id").val() && $("#localidad").val()) {
                            $("#localidad-error-dos").css("display", "inline-block");
                            event.preventDefault();
                            return false;
                        }else{
                            $("#localidad-error-dos").css("display", "none");
                        }                    
                        return true;
                    });
                });
            </script>
            <style type="text/css" media="screen">
                .typeahead__list {
                    max-height: 250px;
                    overflow-y: auto;
                    overflow-x: hidden;
                }
                input.search-input {
                    box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    width: 100%;
                    margin-bottom: 5px;
                    height: auto;
                }
                label.error {
                    color: red;
                    margin-top: 5px;
                    font-weight: normal;
                }
                span.error {
                    color: red;
                    margin-top: 5px;
                    font-weight: normal;
                }
                #pageloader
                {
                  background: rgba( 255, 255, 255, 0.8 );
                  display: none;
                  height: 100%;
                  position: fixed;
                  width: 100%;
                  z-index: 9999;
                }

                #pageloader img
                {
                  left: 50%;
                  margin-left: -32px;
                  margin-top: -32px;
                  position: absolute;
                  top: 50%;
                }
            </style>
            <script type="text/javascript">
                $(document).ready(function(){
                    $("#form_solicitud").on("submit", function(){
                    $("#pageloader").fadeIn();
                  });//submit
                });//document ready
            </script>
        </head>
        <body>
            
        @yield('content')

        <script src="{{ asset('js/jquery.typeahead.min.js') }}"></script>
            <script>
                var provincia = $("#provincia_id").val();
                $(document).ready(function(){
                    if (provincia == "" || provincia == 101){
                        $("#localidad").attr("disabled", true);
                        $("#cont-localidad").css("display", "none");
                        $("#localidad").val("");
                        $("#localidad_id").val("");
                        $("#partido_id").val("");
                    }else{
                        $("#localidad").attr("disabled", false);
                        $("#cont-localidad").css("display", "block");       
                    }
                    $("#provincia_id").change(function () {
                      if ($(this).val() == 101 || $(this).val() == "") {
                        $("#localidad").attr("disabled", true);
                        $("#cont-localidad").css("display", "none");
                        $("#localidad").val("");
                        $("#localidad_id").val("");
                        $("#partido_id").val("");
                      }else{
                        $("#localidad").attr("disabled", false);
                        $("#cont-localidad").css("display", "block");
                        $("#localidad").val("");
                        $("#localidad_id").val("");
                        $("#partido_id").val("");          
                      }
                      provincia=$(this).val();
                    });
                });

                $.typeahead({
                    input: '.js-typeahead',
                    minLength: 0,
                    maxItem: 15,
                    order: "asc",
                    dynamic: true,
                    delay: 300,
                    backdrop: {
                        "background-color": "#fff"
                    },
                    template: function (query, item) {
                        return "<b>"+item.localidad_nombre+"</b>, <small><i>Partido de "+item.partido_nombre+"</i></small>";
                    },
                    emptyTemplate: "No se encontraron resultados",
                    source: {
                        user: {
                            display: "localidad_nombre",
                            data: function () {
                                var deferred = $.Deferred(),
                                    query = this.query;
                                $.getJSON(
                                    '{{ url('solicitud/sLocalidad') }}',
                                    {
                                        q: query,
                                        p: provincia
                                    },
                                    function (data) {
                                        //alert(data);
                                        deferred.resolve(data)
                                    }
                                )
                                return deferred;
                            }
                        },
                    },
                    callback: {
                        onClickAfter: function (node, a, item, event) {
                            $('#localidad_id').val(item.localidad_id);
                            $('.js-typeahead').val(item.localidad_nombre+", "+item.partido_nombre);
                        }
                    },
                    debug: false
                });
            </script>
            <script src="{{ asset('js/jquery.mask.js') }}" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#vfob').mask('#.##0,00', {reverse: true});
                    $('#cdeclarada').mask('#.##0,000', {reverse: true});
                });
            </script>
        </body>
        </html>