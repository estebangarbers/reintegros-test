<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Solicitud de reintegro a las exportaciones N° {{str_pad($solicitud->id, 8, "0", STR_PAD_LEFT)}}</title>
        <style>
            @page{
                margin:30px 50px 100px 50px;
                font-family: 'Arial';
            }
            .cabecera{
                margin-bottom:30px;
                padding-left:7%;
            }
            .col-43{
                width:43%;
            }
            .col-50{
                width:50%;
            }
            .col-100{
                width:100%;
            }
            .pull-left{
                float: left;
            }
            .pull-right{
                float: right;
            }
            .text-right{
                text-align: right;
            }
            .text-left{
                text-align: left;
            }
            p{
                text-align: justify;
            }
            .cuerpo{
                width: 93%;
                float:right;
                padding-left:7%;
                line-height: 1.8em;
            }
        </style>
    </head>
    <body>
        <div class="cabecera">
            <div class="col-43 pull-left">
                <img src="{{ url('img/sec_agroindustria.jpg')}}" width="300" alt="">
            </div>
            <div class="col-50 pull-right text-right" style="font-size:0.6em;margin-top:0;">
                <i>"2019 - AÑO DE LA EXPORTACIÓN"</i>
            </div>
        </div>
        <div class="col-100">
            @yield('content')
        </div>
    </body>
</html>