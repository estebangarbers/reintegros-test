<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')
            ->references('id')->on('empresas');
            $table->string('domicilio');
            $table->integer('localidad_id')->unsigned()->nullable();
            $table->foreign('localidad_id')
            ->references('id')->on('localidades');
            $table->integer('provincia_id')->unsigned();
            $table->foreign('provincia_id')
            ->references('id')->on('provincias');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')
            ->references('id')->on('productos');
            $table->integer('nrestablecimiento');
            $table->double('cdeclarada',19,3);
            $table->integer('paisdestino_id');
            $table->string('parancelaria');
            $table->text('descripcion');
            $table->string('mtransporte');
            $table->string('ntransporte');
            $table->double('vfob',19,2);
            $table->string('otros_registros')->nullable();
            $table->string('destinacion')->nullable();
            $table->string('email');
            $table->string('telefono');
            $table->integer('sesion_id')->unsigned();
            $table->foreign('sesion_id')
            ->references('id')->on('sesions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
