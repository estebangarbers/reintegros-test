<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSesionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sesions', function (Blueprint $table) {
            //$table->dropForeign('empresa_id');
            $table->increments('id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')
            ->references('id')->on('empresas')
            ->onDelete('cascade');
            $table->integer('last_activity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sesions');
    }
}
