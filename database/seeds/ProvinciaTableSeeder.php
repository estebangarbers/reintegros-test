<?php

use Illuminate\Database\Seeder;
use Reintegros\Provincia;

class ProvinciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provincias')->delete();
        $provincias = array(
            array('id' => '97','nombre' => 'BUENOS AIRES'),
            array('id' => '98','nombre' => 'CATAMARCA'),
            array('id' => '99','nombre' => 'CHACO'),
            array('id' => '100','nombre' => 'CHUBUT'),
            array('id' => '101','nombre' => 'CIUDAD DE BUENOS AIRES'),
            array('id' => '102','nombre' => 'CORDOBA'),
            array('id' => '103','nombre' => 'CORRIENTES'),
            array('id' => '104','nombre' => 'ENTRE RIOS'),
            array('id' => '105','nombre' => 'FORMOSA'),
            array('id' => '106','nombre' => 'JUJUY'),
            array('id' => '107','nombre' => 'LA PAMPA'),
            array('id' => '108','nombre' => 'LA RIOJA'),
            array('id' => '109','nombre' => 'MENDOZA'),
            array('id' => '110','nombre' => 'MISIONES'),
            array('id' => '111','nombre' => 'NEUQUEN'),
            array('id' => '112','nombre' => 'RIO NEGRO'),
            array('id' => '113','nombre' => 'SALTA'),
            array('id' => '114','nombre' => 'SAN JUAN'),
            array('id' => '115','nombre' => 'SAN LUIS'),
            array('id' => '116','nombre' => 'SANTA CRUZ'),
            array('id' => '117','nombre' => 'SANTA FE'),
            array('id' => '118','nombre' => 'SANTIAGO DEL ESTERO'),
            array('id' => '119','nombre' => 'TIERRA DEL FUEGO'),
            array('id' => '120','nombre' => 'TUCUMAN')
          );


        DB::table('provincias')->insert($provincias);
    }
}
